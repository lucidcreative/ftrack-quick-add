import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css'

import Example from "./components/GridView";
ReactDOM.render(<Example/>, document.getElementById('root'))

// import App from './components/App';
// ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
