/**
 * tests for insert.js
 * @author KBjordahl
 * 
 */

import * as actions from './listActions';
import { dummyData } from "./util.test";

// tests for selectNext
test("selects next item", () => {
    const index = 2;
    const selected = dummyData[index];
    const updatedData = actions.selectNext(selected.id, dummyData);
    expect(updatedData).toHaveProperty("selectedId", dummyData[index + 1].id);
    expect(updatedData).toHaveProperty(`tree.${index + 1}.selected`, true);
});

test("selects last item when current item is last", () => {
    const index = dummyData.length - 1;
    const selected = dummyData[index];
    const updatedData = actions.selectNext(selected.id, dummyData);
    expect(updatedData).toHaveProperty("selectedId", selected.id);
    expect(updatedData).toHaveProperty(`tree.${index}.selected`, true);
});

// tests for selectPrevious

test("selects previous item", () => {
    const index = 2;
    const selected = dummyData[index];
    const updatedData = actions.selectPrevious(selected.id, dummyData);
    expect(updatedData).toHaveProperty("selectedId", dummyData[index - 1].id);
    expect(updatedData).toHaveProperty(`tree.${index - 1}.selected`, true);
});

test("selects first item when current item is first", () => {
    const index = 0;
    const selected = dummyData[index];
    const updatedData = actions.selectPrevious(selected.id, dummyData);
    expect(updatedData).toHaveProperty("selectedId", selected.id);
    expect(updatedData).toHaveProperty(`tree.${index}.selected`, true);
});

// tests for insertNew
test("inserts new data in correct position", () => {
    const index = 2;
    const selected = dummyData[index];
    const updatedData = actions.insertNew(selected.id, dummyData);
    expect(updatedData).toHaveProperty("tree.2.parentId", selected.parentId);
});