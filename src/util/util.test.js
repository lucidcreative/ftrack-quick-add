/**
 * Tests for util.js
 * @author KBjordahl
 * 
 */

import * as util from "./util";
import { WSAESOCKTNOSUPPORT } from "constants";
export const dummyData = [
    {//0
        id: "a",
        parentId: "root",
    },
    {//1
        id: "b",
        parentId: "a",
    },
    {//2
        id: 'c',
        parentId: "a",
        selected: false
    },
    {//3
        id: "d",
        parentId: "a",
        selected: false
    },
    {//4
        id: "e",
        parentId: "root",
        selected: false
    },
    {//5
        id: "f",
        parentId: "e",
        selected: false
    },
    {//6
        id: "g",
        parentId: "root",
        selected: false
    },
]

test("gets parentId correctly", () => {
    expect(util.getParentId(dummyData[1])).toBe("a")
})

test("sets parentId correctly", ()=>{
    const testIndex = 4;
    expect(dummyData[testIndex].parentId).toBe("root");
    const newTree = util.setParentId(dummyData[testIndex], "a", dummyData);
    expect(newTree[testIndex].parentId).toBe("a");
})

test("gets correct entity by id", ()=>{
    expect(util.getById("c", dummyData).id).toBe("c")
})

test("gets correct index by id", ()=>{
    expect(util.getIndexById("c", dummyData)).toBe(2)
})

test("gets correct peer list", ()=>{
    const parentIndex = 0;
    const peers = util.peerList(dummyData[parentIndex].id, dummyData);
    expect(peers.length).toBe(3);
    expect(peers[0].id).toBe("b");
    expect(peers[1].id).toBe('c');
})

test("determines true top peer correctly", ()=>{
    const entityIndex=1;
    expect(util.isTopPeer(dummyData[entityIndex], dummyData)).toBeTruthy();
})

test("determines non top peer correctly", ()=>{
    const entityIndex=2;
    expect(util.isTopPeer(dummyData[entityIndex], dummyData)).toBeFalsy();
})

test("determines lone peer as top correctly", ()=>{
    const entityIndex=5;
    expect(util.isTopPeer(dummyData[entityIndex], dummyData)).toBeTruthy();
})

test("determines true bottom peer correctly", ()=>{
    const entityIndex=3;
    expect(util.isBottomPeer(dummyData[entityIndex], dummyData)).toBeTruthy();
})

test("determines non top peer correctly", ()=>{
    const entityIndex=2;
    expect(util.isBottomPeer(dummyData[entityIndex], dummyData)).toBeFalsy();
})

test("determines lone peer as bottom correctly", ()=>{
    const entityIndex=5;
    expect(util.isBottomPeer(dummyData[entityIndex], dummyData)).toBeTruthy();
})

test("gets peer above correctly for multiple peers", ()=>{
    const entityIndex=2;
    const peer = util.getPeerAbove(dummyData[entityIndex], dummyData);
    expect(peer).toBe(dummyData[entityIndex-1]);
})

test("gets self as peer above correctly for lone peer", ()=>{
    const entityIndex=5;
    const peer = util.getPeerAbove(dummyData[entityIndex], dummyData);
    expect(peer).toBe(dummyData[entityIndex]);
})

test("gets peer below correctly for multiple peers", ()=>{
    const entityIndex=2;
    const peer = util.getPeerBelow(dummyData[entityIndex], dummyData);
    expect(peer).toBe(dummyData[entityIndex+1]);
})

test("gets self as peer above correctly for lone peer", ()=>{
    const entityIndex=5;
    const peer = util.getPeerBelow(dummyData[entityIndex], dummyData);
    expect(peer).toBe(dummyData[entityIndex]);
})

test("gets correct last child index for multiple peers", ()=>{
    const entityIndex = 0;
    const lastIndex = util.getLastChildIndex(entityIndex, dummyData);
    expect(lastIndex).toBe(3);
})

test("correctly builds dataTemplate with no input", ()=>{
    expect(util.dataTemplate({})).toHaveProperty('parentId', "root")
})

test("correctly builds dataTemplate with provided input", ()=>{
    const index = 2;
    const entity = dummyData[index];
    expect(util.dataTemplate(entity)).toHaveProperty('parentId', entity.parentId)
})

test("correctly sets the selected entity in the tree", ()=>{
    const selectedIndex = 2;
    const entity = dummyData[selectedIndex]
    const newTree = util.setSelectedInTree(entity.id, dummyData);
    expect(newTree).toHaveProperty(`${selectedIndex}.selected`, true)
})