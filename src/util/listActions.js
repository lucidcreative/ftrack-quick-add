/**
 * listActions
 * @author KBjordahl
 * @description actions for adding and deleting list elements
 */

const R = require('ramda');
const util = require('./util');
const treis = require('treis');

/**
 * @function selectNext
 * @author KBjordahl
 * @param currentSelectedId {string}: Currently selected ID
 * @param data {array}: the data array
 * @returns {Object}: state mergeable object with selectedId as the id of the next element
 */
// (String, [Object]) -> String
export function selectNext(currentSelectedId, data){
    const newId = R.compose(
        R.prop('id'),
        R.nth(R.__, data),
        R.min(R.dec(R.length(data))),
        R.inc,
        util.getIndexById(currentSelectedId)
    )(data)
    const newTree = util.setSelectedInTree(newId, data)
    return {tree: newTree, selectedId: newId}
}

/**
 * @function selectPrevious
 * @author KBjordahl
 * @param currentSelectedId {string}: Currently selected ID
 * @param data {array}: the data array
 * @returns {Object}: state mergeable object with selectedId as the id of the previous element
 */

// (String, [Object]) -> Object
export function selectPrevious(currentSelectedId, data){
    const newId = R.compose(
        R.prop('id'),
        R.nth(R.__, data),
        R.max(0),
        R.dec,
        util.getIndexById(currentSelectedId)
    )(data)
    const newTree = util.setSelectedInTree(newId, data)
    return {tree: newTree, selectedId: newId}
}

/**
 * @function insertNew
 * @author KBjordahl
 * @param newDataTemplate {function}: the data template to be inserted, as a function 
 * @param currentSelectedId {string}: Currently selected ID
 * @param data {array}: the data array
 * @returns {[Object]}: the id of the previous element
 */

// Object -> String -> [Object] -> {[Object]}
export const insertNew = R.curry(function insertNew(currentSelectedId, data){
    const newEntity = util.dataTemplate(util.getById(currentSelectedId, data))
    const newTree = util.setSelectedInTree(newEntity.id, 
        R.compose(
            R.insert(R.__,newEntity,data),
            R.inc,
            util.getLastChildIndex(R.__, data),
            util.getIndexById(currentSelectedId)
        )(data)
    )
    return ({
        tree: newTree,
        selectedId: newEntity.id
    })
})