'use strict'
/**
 * Mover functions
 * @author KBjordahl
 * @description pure functions to move elements in the tree
 */
import { curry, compose, __, propOr, ifElse, prop, when } from "ramda";
import treis from "treis";

import {
    getById,
    getParentId,
    setParentId,
    putEntityAbove,
    putEntityBelow,
    isTopPeer,
    isBottomPeer,
    getPeerAbove,
    getPeerBelow,
    cascadeChildren,
} from './util';

// movements

/**
 * @function moveLeft
 * @description Moves an entity to the left. 
 * This moves the entity to become a peer of its current parent.
 * @sig string -> [object] -> [object]
 */
// [obj] -> string -> [obj]
export const moveLeft = curry((selectedId, tree) => {
    const entity = getById(selectedId, tree);
    const newParentId = getParentId(getById(getParentId(entity)));
    return compose(
        setParentId(entity, newParentId),
        treis(putEntityBelow(getParentId(entity), selectedId))
    )(tree)
})

/**
 * @function moveRight
 * @description Moves an entity to the right. 
 * This moves the entity to become a child of the peer above it.
 * @sig string, [object] -> [object]
 */
// string, [object] -> [object]
export const moveRight = curry((selectedId, tree) => {
    const entity = getById(selectedId, tree);
    return compose(
        //makeLastPeer(selectedId),
        setParentId(entity, __, tree),
        propOr("root", "id"),
        ifElse(
            isTopPeer(entity),
            getById(prop('parentId', entity)),
            getPeerAbove(entity)
        ),
    )(tree)
})

/**
 * @function moveUp
 * @description Moves an entity up
 * If the entity is the top in it's peer list, then simply move it left
 * @sig (string, [object] -> [object])
 */

export const moveUp = (selectedId, tree) => {
    let entity = getById(selectedId, tree);
    const listIfTopPeer = when(
        isTopPeer(entity),
        moveLeft(selectedId),
    )(tree)
    entity = getById(selectedId, listIfTopPeer)
    return compose(
        cascadeChildren(selectedId),
        putEntityAbove(__, selectedId, listIfTopPeer),
        prop('id'),
        getPeerAbove(entity),
    )(listIfTopPeer)
}

/**
 * @function moveDown
 * @description Moves an entity down
 * If the entity is the bottom in it's peer list, then simply move it left
 * @sig (string, [object] -> [object])
 */

export const moveDown = (selectedId, tree) => {
    let entity = getById(selectedId, tree);
    const listIfBottomPeer = when(
        isBottomPeer(entity),
        moveLeft(selectedId),
    )(tree)
    entity = getById(selectedId, listIfBottomPeer)
    return compose(
        cascadeChildren(selectedId),
        putEntityBelow(__, selectedId, listIfBottomPeer),
        prop('id'),
        getPeerBelow(entity),
    )(listIfBottomPeer)
}