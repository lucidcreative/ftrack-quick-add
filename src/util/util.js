/**
 * Data Helpers
 * @author KBjordahl
 * @description pure functions to help with some data stuff
 */

const R = require( "ramda" );
const uuid = require('uuid/v4');
const treis = require( "treis" );

// Object -> String
export const getParentId = R.propOr("root", "parentId")

// object, string, [object] -> [object]
export const setParentId = R.curry((entity, newParent, tree)=>(
    R.set(parentIdLens(entity, tree), newParent, tree)
))

export const parentIdLens = (entity, tree) => ( 
    R.lensPath([R.indexOf(entity,tree), 'parentId'])
)

// ([Object], String) -> Object
export const getById = R.curryN(2, (id, data)=>{
    return R.head(R.filter(R.propEq('id', id), data));
})

/**
 * @param id {string}: ID of the target object
 * @param data {array}: the data array
 * @returns {number}: the index of the id 
 */
// ([Object], String) -> Object
export const getIndexById = R.curry(function getIndexById(id, data){
    const item = getById(id, data);
    return R.indexOf(item, data);
})

// ([Object], String) -> [Object]
export const peerList = R.curryN(2, (parentId, data)=>(
    R.filter(R.propEq("parentId", parentId), data)
))

// ([Object], Object) -> Boolean
export const isTopPeer = R.curry( function isTopPeer(entity, data){ return (
    R.compose(R.equals(0),
        R.indexOf(entity),
        peerList(R.__, data),
        R.prop("parentId")
    )(entity)
)})

// ([Object], Object) -> Boolean
export const isBottomPeer = R.curry( function isTopPeer(entity, data){
    const peers = peerList(R.prop("parentId", entity), data);
    return R.compose(
        R.equals(R.length(peers)),
        R.inc,
        R.indexOf(entity),
    )(peers)
})

// object -> [object] -> object
export const getPeerAbove = R.curry( function getPeerAbove(entity, data){ 
    const peers = peerList(entity.parentId, data);
    return R.compose(
        R.nth(R.__,peers),
        R.max(0),
        R.dec,
        R.indexOf(entity)
     )(peers)
})

// object -> [object] -> object
export const getPeerBelow = R.curry( function getPeerBelow(entity, data){ 
    const peers = peerList(entity.parentId, data);
    return R.compose(
        R.nth(R.__,peers),
        R.min(R.dec(R.length(peers))),
        R.inc,
        R.indexOf(entity)
     )(peers)
})

// int -> [object] -> int
export const getLastChildIndex = R.curry( function getLastChildIndex(index, data){
    const parent = data[index];
    const children = R.filter(R.propEq('parentId', R.prop('id', parent)), data)
    return R.length(children) === 0 ? index : 
        getLastChildIndex(R.indexOf(R.last(children),data), data)        
})

// export const swap = R.curry((index1, index2, list) => {
//     if (index1 < 0 || index2 < 0 || index1 > list.length - 1 || index2 > list.length - 1) {
//       return list // index out of bound
//     }
//     const value1 = list[index1]
//     const value2 = list[index2]
//     return R.pipe(
//       R.set(R.lensIndex(index1), value2),
//       R.set(R.lensIndex(index2), value1)
//     )(list)
//   })

// /**
//  * @function swapEntities
//  * @description swaps entities in a list. Each entity should be unique in the list
//  * @sig object -> object -> list -> list
//  */

// export const swapEntities = R.curry((entity1, entity2, list) => {
//     const index1 = R.indexOf(entity1, list);
//     const index2 = R.indexOf(entity2, list);
//     return swap(index1, index2, list);
// })

export const putEntityAbove = R.curry((anchorId, movingId, list) => {
    const anchor = getById(anchorId, list);
    const moving = getById(movingId, list);
    const cleanList = R.reject(R.equals(moving), list);
    return R.insert( R.indexOf(anchor, cleanList),moving , cleanList);
})

export const putEntityBelow = R.curry((anchorId, movingId, list) => {
    const anchor = getById(anchorId, list);
    const moving = getById(movingId, list);
    const cleanList = R.reject(R.equals(moving), list);
    const index = getLastChildIndex(R.indexOf(anchor, cleanList), cleanList);
    return R.insert(R.inc(index) ,moving , cleanList);
})

export const makeLastPeer = R.curry((entityId, list)=>{
    const entity = getById(entityId, list);
    const peers = R.filter(R.eqProps('parentId', entity),list)
    const anchor = R.ifElse(
        R.compose(R.equals(1), R.length),
        R.compose(
            R.prop('parentId'),
            getById(entityId)
        ),
        R.compose(
            R.prop('id'),
            R.last
        )
    )(peers);
    return putEntityBelow(anchor, entityId, list)   
})

export const cascadeChildren = R.curry(function cascadeChildren(parentId, list){
    const newList = R.compose(
        R.reduce(
            (acc, child)=>{
                console.log(`child ${child.id}, parent ${parentId}`)
                console.log(acc)
                const id = R.prop('id', child);
                if( id === parentId){ return acc }
                const reparented = putEntityBelow(parentId, id, acc);
                const reordered = makeLastPeer(id, reparented);
                return cascadeChildren(id, reordered)
                return R.compose(
                    cascadeChildren(id),
                    makeLastPeer(id),
                    putEntityBelow(parentId, id),
                )(acc);
            }, 
            list),
        R.filter(R.propEq('parentId', parentId))
    )(list)
    return newList
})

export function dataTemplate({parentId="root"}={}) {
    return { 
        id: uuid(),
        name: "",
        class: "",
        type: "",
        parentId: parentId,
        selected: false,
    }
}

export function setSelectedInTree(selectedId, data){
    const newTree = R.map(
        (item)=>({...item, selected: item.id == selectedId, }), 
        data);
    return newTree;
}