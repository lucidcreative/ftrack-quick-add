/* global window, document, CustomEvent */

import React from 'react';
import ReactDOM from 'react-dom';
import log from 'loglevel';
import ftrackWidget from 'ftrack-web-widget';
import { Session } from 'ftrack-javascript-api';

import Root from './components/GridView'

import 'bootstrap/dist/css/bootstrap.css'

import './mocked_widget_api.js';

// eslint-disable-next-line
export let session = null;

function onWidgetUpdate() {
    const entity = ftrackWidget.getEntity();
    const event = new CustomEvent('ftrackWidgetUpdate', { detail: { entity } });
    window.dispatchEvent(event);
}

function onWidgetLoad() {
    const credentials = ftrackWidget.getCredentials();
    session = new Session(
        credentials.serverUrl,
        credentials.apiUser,
        credentials.apiKey
    );

    log.debug('Initializing API session.');
    session.initializing.then(() => {
        log.debug('Session initialized');

        ReactDOM.render(
            <Root />,
            document.getElementById('root')
        );
    });

    onWidgetUpdate();
}

/** Initialize widget once DOM has loaded. */
function onDomContentLoaded() {
    log.debug('DOM content loaded, initializing widget.');
    ftrackWidget.initialize({
        onWidgetLoad,
        onWidgetUpdate,
    });
}

window.addEventListener('DOMContentLoaded', onDomContentLoaded);
