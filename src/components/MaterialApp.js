import React from "react";
import * as R from 'ramda';

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import {amber, grey, teal} from 'material-ui/colors'
import { createMuiTheme, CssBaseline } from "material-ui";
import { FormControl } from "material-ui/Form";

import EntityList from "./EntityList";
import * as util from "../util/"
import * as listActions from "../util/listActions";

// TODO: change this eventually
const winston = require('winston');
winston.configure({
    transports: [
      new winston.transports.Console()
    ],
    level: 'debug',
});

const theme = createMuiTheme({
    palette: {
        primary: teal,
        secondary: grey,
        error: amber,
        type: 'dark'
    },
    overrides: {
        MuiCard: {
            root: {
                // background: teal[500],
                overflow: "none",
                margin: 8
            }
        }
    }
  
});

const classes = [
    {
        name: "Folder",
        id: "5kjwbesnjnsejk",
        types: [
            { 
                id: "234nfw3k",
                name:"generic",
            },
            {
                id: "9dfknwf9",
                name: "Content Management",
            }
        ]
    },
    {
        name: "Epic",
        id: "epicnoodle",
        types: [
            { 
                id: "sdkfn",
                name:"generic",
            },
            {
                id: "vesergg",
                name: "Scrum Management",
            }
        ]
    },
]



const dummyData = [
    { 
        id: "1",
        name: "Folder 1",
        class: "5kjwbesnjnsejk",
        type: "234nfw3k",
        parentId: "root",
        selected: true,
    },   
    {
        id: "2",
        name: "Subfolder 1.1",
        class: "5kjwbesnjnsejk",
        type: "generic",
        parentId: "1",
    },
    {
        id: '3',
        name: "Folder 2",
        class: "5kjwbesnjnsejk",
        type: "generic",
        parentId: "root",
    },
    {
        id: "4",
        name: "Subfolder 2.1",
        class: "5kjwbesnjnsejk",
        type: "generic",
        parentId: "root",
    },
]

export class App extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            tree: dummyData,
            classes: [],
            selectedId: "1",
            selectedField: 0,
            preventKeys: false
        }
    }

    componentWillMount(){
        // get keypresses
        document.addEventListener('keydown', this.handleKeyDown, false);
    }
    componentWillUnmount(){
        // end keypresses
        document.removeEventListener('keydown', this.handleKeyDown, false);
    }

    componentDidMount(){

        // get types and such from ftrack
    }

    handleChange = ({id, field}) => e => {
        // map over the state, replace object whose id matches our given id
        const newTree = this.state.tree.map((item)=>{
            return item.id === id ? 
                {...item, [field]:e.target.value}:
                item
        });

        this.setState(()=>({
            tree : newTree
        }));
    }

    handleSubmit = (e)=>{
        e.preventDefault();
    }

    handleKeyDown = (e)=>{
        console.log(e.key)
        if (this.state.preventKeys) {
            winston.debug("Keys prevented!");
            return;
        }
        if (e.ctrlKey && false) { // false is blocking the use of these!
            let movementFunction = undefined;
            switch(e.key){
                case "ArrowLeft":
                    movementFunction = util.moveLeft;
                    break; 
                case "ArrowRight":
                    movementFunction = util.moveRight;
                    break;
                case "ArrowUp":
                    movementFunction = util.moveUp;
                    break;
                case "ArrowDown":
                    movementFunction = util.moveDown;
                    break;
            }
            if ( movementFunction ){
                this.setState(()=>({
                    tree: movementFunction(this.state.selectedId, this.state.tree),
                    //selectedId: targetIndex,
                }));
            }
        } else {
            // actions for moving selection up and down
            let action;
            switch(e.key){
                case "ArrowUp":
                    // select previous
                    action = listActions.selectPrevious;
                    break;

                case "ArrowDown":
                    action = listActions.selectNext;
                    break;

                case "Enter":
                    action = listActions.insertNew;
                    break;
            }
            if ( action ){
                console.log(action.name)
                this.setState(()=>(action(
                    this.state.selectedId,
                    this.state.tree
                )))
            } 
        }
        
    }

    handleNewFocus=(e)=>{
        console.log(e.target);
        console.log(e.currentTarget);
        // this gets the id of the data item which has been selected
        const selectedId = e.currentTarget.id.split("-")[1]
        const tree = this.state.tree.map((item, index)=>{
            const selected = item.id === selectedId
            return {...item, selected}
        })

        this.setState(()=>({
            tree,
            selectedId,
        }))
    }

    preventKeys = (keyState)=>{
        if (keyState==undefined) { return this.state.preventKeys }
        console.log(`preventing keys: ${keyState}`)
        this.setState(()=>({preventKeys: keyState}))
    }


    render(){ 
        return (
            <React.Fragment>
                <CssBaseline/>
                <MuiThemeProvider theme={theme}>
                    <form 
                        onSubmit={this.handleSubmit}                         
                        >
                        <EntityList 
                                data={this.state.tree}
                                id="root"
                                classes={classes}
                                handleChange={this.handleChange}
                                focusCallback={this.handleNewFocus}
                                preventKeysFn={this.preventKeys}
                                /> 
                    </form> 
                </MuiThemeProvider>
            </React.Fragment>
    )}
}

export default App;

/*
                    { Object.keys(this.state.tree).filter(item=>this.state.tree[item].parent_id===undefined)
                        .map((key)=>{
                            const entity = this.state.tree[key];
                            return (
                                <EntityRow 
                                    entity={entity} 
                                    key={entity.id}
                                    handleChange={this.handleChange}
                                    classes={classes}
                                    data={this.state.tree}
                                    />
                            )
                        })
                    }
 */