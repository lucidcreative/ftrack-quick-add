import ReactDataGrid from "react-data-grid";
import React from "react";
import { Editors, Formatters } from "react-data-grid-addons";
import update from 'immutability-helper';

import * as R from 'ramda';

import * as util from '../util/util'

const { AutoComplete: AutoCompleteEditor, DropDownEditor } = Editors;
const { DropDownFormatter } = Formatters;

// options for priorities autocomplete editor
const priorities = [{ id: 0, title: 'Critical' }, { id: 1, title: 'High' }, { id: 2, title: 'Medium' }, { id: 3, title: 'Low'} ];
const PrioritiesEditor = <AutoCompleteEditor 
    options={priorities} 
    
  />;

// options for IssueType dropdown editor
// these can either be an array of strings, or an object that matches the schema below.
const issueTypes = [
  { id: 'bug', value: 'bug', text: 'Bug', title: 'Bug' },
  { id: 'improvement', value: 'improvement', text: 'Improvement', title: 'Improvement' },
  { id: 'epic', value: 'epic', text: 'Epic', title: 'Epic' },
  { id: 'story', value: 'story', text: 'Story', title: 'Story' }
];
const IssueTypesEditor = <DropDownEditor options={issueTypes}/>;

const IssueTypesFormatter = <DropDownFormatter options={issueTypes} value="bug"/>;

const classDropdownEditor = (rowArgs) => {
  console.log(rowArgs);
  return <DropDownEditor options={[]}/>
}

class ListController extends React.Component {
  constructor(props, context) {
    super(props, context);
    
    this._columns = [
      {
        key: 'obj',
        name: 'Object',
        editor: classDropdownEditor
      },
      {
        key: 'name',
        name: 'Name',
        editable: true,
      },
      {
        key: 'objectType',
        name: 'Object Type',
        // editor: this.typeDropdownEditor,
      }
    ];

    this.state = { 
      rows: [util.dataTemplate()],
      classTypes: [
        {
          text: "Loading", 
          title: "Loading",
          id:"loading", 
          value:"loading", 
          types: [
            {
              name: "Loading", 
              id:"loading"
            }
          ]
        }
      ]
    };
  }

  // classDropdownEditor = (rowArgs) => {
  //   console.log(rowArgs);
  //   return <DropDownEditor options={[]}/>
  // }

  // typeDropdownEditor = (rowArgs) => {
  //   console.log(rowArgs);
  //   return (
  //     <DropDownEditor options={R.compose(
  //       R.prop("types"), 
  //       R.head,
  //       R.filter(R.propEq("id", 'loading'))
  //       )(this.state.classTypes)}
  //       />
  //   )
  // }

  rowGetter = (i) => {
    return this.state.rows[i];
  };

  handleGridRowsUpdated = ({ fromRow, toRow, updated }) => {
    let rows = this.state.rows.slice();

    for (let i = fromRow; i <= toRow; i++) {
      let rowToUpdate = rows[i];
      let updatedRow = update(rowToUpdate, {$merge: updated});
      rows[i] = updatedRow;
    }

    this.setState({ rows });
  };

  render() {
    return (
      <ReactDataGrid
        enableCellSelect={true}
        cellNavigationMode={'loopOverRow'}
        columns={this._columns}
        rowGetter={this.rowGetter}
        rowsCount={this.state.rows.length}
        minHeight={500}
        onGridRowsUpdated={this.handleGridRowsUpdated} />);
  }
}

export default ListController;