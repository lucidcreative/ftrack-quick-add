import React from 'react';
import PropTypes from 'prop-types';

import EntityRow from "./EntityRow";


class EntityList extends React.Component {
    render() {
        const { id, data, classes, handleChange, focusCallback, preventKeysFn } = this.props
        return (
            <React.Fragment>
            { Object.keys(data).filter(item=>data[item].parentId===id)
                .map((key)=>{
                    const entity = data[key];
                    return (
                        <EntityRow 
                            entity={entity} 
                            key={entity.id}
                            classTypes={classes}
                            data={data}
                            handleChange={handleChange}
                            focusCallback={focusCallback}
                            preventKeysFn={preventKeysFn}
                            />
                            
                )}
            )}
            </React.Fragment>
        )
    }
}

EntityList.propTypes = {
    id: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired,
    classes: PropTypes.array.isRequired,
    handleChange: PropTypes.func.isRequired,
    preventKeysFn: PropTypes.func.isRequired,
}

export default EntityList;