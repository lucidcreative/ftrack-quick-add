import React from "react";
import * as R from 'ramda';

export class App extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            tree: [],
            classes: [],
            selectedId: "1",
            selectedField: 0,
            preventKeys: false
        }
    }

    componentWillMount(){
        // get keypresses
        document.addEventListener('keydown', this.handleKeyDown, false);
    }
    componentWillUnmount(){
        // end keypresses
        document.removeEventListener('keydown', this.handleKeyDown, false);
    }

    componentDidMount(){

        // get types and such from ftrack
    }

    handleChange = ({id, field}) => e => {
        // map over the state, replace object whose id matches our given id
        const newTree = this.state.tree.map((item)=>{
            return item.id === id ? 
                {...item, [field]:e.target.value}:
                item
        });

        this.setState(()=>({
            tree : newTree
        }));
    }

    handleSubmit = (e)=>{
        e.preventDefault();
    }

    handleKeyDown = (e)=>{
        console.log(e.key)
        if (this.state.preventKeys) {
            winston.debug("Keys prevented!");
            return;
        }
        if (e.ctrlKey && false) { // false is blocking the use of these!
            let movementFunction = undefined;
            switch(e.key){
                case "ArrowLeft":
                    movementFunction = util.moveLeft;
                    break; 
                case "ArrowRight":
                    movementFunction = util.moveRight;
                    break;
                case "ArrowUp":
                    movementFunction = util.moveUp;
                    break;
                case "ArrowDown":
                    movementFunction = util.moveDown;
                    break;
            }
            if ( movementFunction ){
                this.setState(()=>({
                    tree: movementFunction(this.state.selectedId, this.state.tree),
                    //selectedId: targetIndex,
                }));
            }
        } else {
            // actions for moving selection up and down
            let action;
            switch(e.key){
                case "ArrowUp":
                    // select previous
                    action = listActions.selectPrevious;
                    break;

                case "ArrowDown":
                    action = listActions.selectNext;
                    break;

                case "Enter":
                    action = listActions.insertNew;
                    break;
            }
            if ( action ){
                console.log(action.name)
                this.setState(()=>(action(
                    this.state.selectedId,
                    this.state.tree
                )))
            } 
        }
        
    }


    render(){
        return (

        )
    }