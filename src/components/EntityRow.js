import React from "react";
import PropTypes from 'prop-types';
import Card, { CardContent } from "material-ui/Card";
import TextField from "material-ui/TextField";

import EntityList from './EntityList';
import AutocompleteSelector from "./AutocompleteSelector";
import { withTheme } from "material-ui";


class EntityRow extends React.Component {

    handleChange = control => e => {

        this.props.handleChange(control)(e)
    }

    handleSelectChange = control => value => {
        if (value === null) { value = "" } 
        const e = { target: { value } };
        this.handleChange(control)(e);
    }

    render(){ 
    
        const { theme, entity, classTypes, data, focusCallback, preventKeysFn } = this.props;
        
        return (
        <Card 
            raised={entity.selected} 
            style={{backgroundColor: entity.selected ? 
                theme.palette.primary[ theme.palette.type==='light'?'300':'700'] :
                theme.palette.background.paper
                }}
            key={entity.id}>
            <CardContent>
                <div 
                    id={"selection-"+entity.id}
                    onFocus={focusCallback}
                >
                {
                    // class selector
                }
                <AutocompleteSelector
                    options={classTypes}
                    style={{width: "25%", paddingRight: theme.spacing.unit*2}}
                    placeholder="class"
                    value = {entity.class}
                    handleChange={this.handleSelectChange({id: entity.id, field: "class"})}
                    preventKeysFn={preventKeysFn}
                    />
                {
                    // name field
                }
                <TextField
                    id={`entity-name-${entity.id}`}
                    placeholder="name"
                    value={entity.name}
                    onChange={this.handleChange({id: entity.id, field: "name"})}
                    margin="normal"
                    style={{width:'45%'}}
                    />
                {
                    // type selector
                }
                <AutocompleteSelector
                    options={   // get the types from the selected class
                        entity.class ? classTypes.filter(item=>item.id===entity.class)[0].types:
                            []
                    }
                    style={{width: "25%", paddingLeft: theme.spacing.unit*2}}
                    placeholder="type"
                    value = {entity.type}
                    handleChange={this.handleSelectChange({id: entity.id, field: "type"})}
                    preventKeysFn={preventKeysFn}
                    />
                </div>

                <EntityList 
                    id={entity.id} 
                    data={data} 
                    classes={classTypes} 
                    handleChange={this.handleChange} 
                    focusCallback={focusCallback}
                    preventKeysFn={preventKeysFn}
                    />
            </CardContent>
        </Card>
    )}
}

const classProps = PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    types: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
    }))
})

const entityProps = PropTypes.shape({
    id: PropTypes.string.isRequired,
    class: PropTypes.string,
    name: PropTypes.string,
    parent: PropTypes.string,
    selected: PropTypes.bool,
});

EntityRow.propTypes = {
    entity: entityProps.isRequired,
    // children are not required, but if they exist, their forms are 
    data: PropTypes.arrayOf(entityProps),
    classTypes: PropTypes.arrayOf(classProps).isRequired,
    handleChange: PropTypes.func.isRequired,
    theme: PropTypes.object.isRequired,
    preventKeysFn: PropTypes.func.isRequired,
}

export default withTheme()(EntityRow);
